﻿##﻿v0.1.2  

Diese Bibliothek steht unter der LGPL Lizenz

Diese Bibliothek ist für python 3.xx und pygame 1.9.2

---

pygame (LGPL Lizenz) http://www.pygame.org/news.html

Installation:

---
pygame in ___Ubuntu___ Linux installieren:
```
sudo apt-get install mercurial python3-dev python3-numpy ffmpeg \
    libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev \
    libsdl1.2-dev  libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev
```
 
## Grab source ##
`hg clone https://bitbucket.org/pygame/pygame`
 
## Finally build and install ##
```
cd pygame
python3 setup.py build
sudo python3 setup.py install
```
---

zur Verwendung muss der Ordner pyogi in das Projekt Verzeichnis eingefügt werden