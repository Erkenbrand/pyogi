from pyogi.locals import REFLECT_SIDE


def too(brother, widget, side="midtop", anchor=None, offset=(0, 0)):
    """
    Platziert ein Widget an ein anderes (brother).
    brother : Giebt das Widget an, an das angeheftet wird.
    side : Gibt die Seite des Widget an, die angeheftet wird.
    anchor : Gibt die Seite an, an die das Widget angeheftet wird.
    offset : Wird zur Position hinzugefuegt.-
    """

    brother_pos = (brother.get_side(anchor) if anchor
                   else brother.get_side(REFLECT_SIDE[side]))
    brother._master.place(widget,
                          pos=(brother_pos[0] + offset[0],
                               brother_pos[1] + offset[1]),
                          side=side, anchor="topleft")

def in_row(container, widgetlist, pos=(0, 0), side="topleft", anchor=None,
           from_side="topleft", too_side="bottomleft", offset=(0, 0)):
    """
    Platziert eine liste von Widgets in eine reihen folge aneinander.
    side : Gibt die Seite an, von der das erste Widget angeheftet wird.
    anchor : Gibt die Seite an, an die das erste Widget angeheftet wird.
    from_side : Gibt die Seite an, von der in reihe angeheftet wird.
    too_side : Gibt die Seite an, an die in reihe angeheftet wird.
    """

    last = widgetlist[0]
    container.place(last, pos, side, anchor)
    for widget in widgetlist[1:]:
        too(last, widget, from_side, too_side, offset)
        last = widget
