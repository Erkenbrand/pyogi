from pyogi.locals import MouseIn


class Rect(object):

    @property
    def size(self):
        return self.rect.size

    @size.setter
    def size(self, vaule):
        self.rect.size = vaule
        self.change(2)

    @property
    def w(self):
        return self.rect.w

    @w.setter
    def w(self, vaule):
        self.rect.w = vaule
        self.change(2)

    @property
    def h(self):
        return self.rect.h

    @h.setter
    def h(self, vaule):
        self.rect.h = vaule
        self.change(2)

    @property
    def x(self):
        return self.rect.x

    @x.setter
    def x(self, vaule):
        self.rect.x = vaule
        self.change(1)

    @property
    def y(self):
        return self.rect.y

    @y.setter
    def y(self, vaule):
        self.rect.y = vaule
        self.change(1)

    def get_side(self, side):
        """gibt die Position des Widget ueber eine Seite aus"""

        return getattr(self.rect, side)

    def master_side(self, side, master_side="topleft"):
        """gibt die Position des Widget abhaengig zur Master Seite aus"""

        if not self._master:
            return (0, 0)
        master_x, master_y = self._master.relativ_side(master_side)
        pos_x, pos_y = self.get_side(side)
        return (pos_x - master_x, pos_y - master_y)

    def set_side(self, pos, side):
        """Setzt die Position des Widget ueber die angegebene Seite."""

        setattr(self.rect, side, pos)
        self.change(1)

    def relativ_pos(self, pos):
        """Gibt die Position von pos im Widget zurueck."""

        return self._master.relativ_pos(self.relativ_master_pos(pos))

    def relativ_master_pos(self, pos):
        """Gibt die Position von pos im Widget im Bezug zum Master zurueck."""

        return (pos[0] - self.rect.x, pos[1] - self.rect.y)

    def relativ_side(self, side="center"):
        """Gibt die Position von einner Seite im Widget zurueck."""

        return self.relativ_master_pos(self.get_side(side))

    def move(self, way=(0, 0)):
        """Bewegt das Widget auf dem Master um den angegebenen Weg."""

        self.rect.move_ip(way)
        self.change(1)

    def move_in(self, rect, way=(0, 0)):
        """
        Bewegt das Widget auf dem master um den angegebenen Weg,
        solange es im uebergebenen rect ist.
        Ist es nicht mehr im rect, wird False zurueckgegeben, (ansonsten True)
        und das Widget wird nicht bewegt.
        """

        new = self.rect.move(way)
        if rect.contains(new):
            self.rect = new
            self.change(1)
            return True
        else:
            return False

    def move_in_master(self, way=(0, 0)):
        """
        Bewegt das Widget auf dem master um den angegebenen weg,
        solange es im master ist.
        Ist es nicht mehr im master, wird False zurueckgegeben, (ansonsten True)
        und das Widget wird nicht bewegt.
        """

        return self.move_in(self._master.rect, way)

    def if_in(self, pos):
        """Testet ob pos im Widget liegt."""

        if self.visibility:
            if self.rect.collidepoint(pos):
                self.mouse_pos = self.relativ_master_pos(pos)
                raise MouseIn(self)
