from pygame import Rect


def bound_rect(widgets):
    """Gibt ein Boundingrect aus das alle uebergebenne Widget umschließt"""

    first = widgets[0]
    min_x = first.x
    min_y = first.y
    max_x, max_y = first.rect.bottomright
    for widget in widgets[1:]:
        x, y = widget.rect.bottomright
        min_x = min(widget.x, min_x)
        min_y = min(widget.y, min_y)
        max_x = max(max_x, x)
        max_y = max(max_y, y)
        first = widget
    return Rect((min_x, min_y), (max_x - min_x, max_y - min_y))

def max_size(widgets):
    """Gibt die Groesse zurueck aus der weitersten weite und hoechsten Hoehe"""

    first = widgets[0]
    width, height = first.size
    for widget in widgets[1:]:
        w, h = widget.size
        width = max(width, w)
        height = max(height, h)
    return (width, height)

def max_width(widgets):
    """Gibt die groesste weite der uebergebennen Widget zurueck"""

    width = 0
    for widget in widgets:
        width = max(width, widget.w)
    return width

def max_height(widgets):
    """Gibt die groesste hoehe der uebergebennen Widget zurueck"""

    height = 0
    for widget in widgets:
        height = max(height, widget.h)
    return height
