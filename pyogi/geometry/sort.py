def row(widgets, pos=(0, 0), offset=(0, 0), from_side="topleft",
        too_side="bottomleft"):
    """Sortiert die Widget von from_side nach too_side"""

    first = widgets[0]
    first.set_side(pos, from_side)
    for widget in widgets[1:]:
        x, y = first.get_side(too_side)
        pos = (offset[0] + x, offset[1] + y)
        widget.set_side(pos, from_side)
        first = widget
