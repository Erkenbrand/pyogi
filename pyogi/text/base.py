from pyogi.basics import Widget
from pyogi.geometry import geometry


class  BaseText(Widget, geometry.Rect):

    def __init__(self, text, font, **config):
        """Einzeiliges Text Widget auf einer Farbe abgebildet"""

        Widget.__init__(self, **config)

        self._fg = None
        self._bg = None
        self._text = text
        self.font = font
        self.rect = self.font.get_rect(text)

    def create(self):
        Widget.create(self)

        self._fg = self._config.get("text_fg", self._theme.colors.text_fg)
        self._bg = self._config.get("text_bg", self._theme.colors.text_bg)

    def draw(self, buffer):
        """zeichnet auf das uebergebene Surface"""

        Widget.draw(self)
        if self.visibility:
            buffer.blit(self.font.render(self.text, self._fg, self._bg)[0],
                        self.rect)

    @property
    def text(self):

        return self._text

    @text.setter
    def text(self, value):

        self._text = value
        self.size = self.font.get_rect(value).size
        self.change(1)

    def __len__(self):

        return len(self._text)

    def __bool__(self):
        return True

    def get_surface(self):
        """giebt den text als Surface zurueck"""

        return self.font.render(self.text, self._fg, self._bg)[0]
