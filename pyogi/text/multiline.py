import pygame

from pyogi.basics import Widget
from pyogi.geometry import geometry, sort, sizes
from pyogi.locals import MouseIn
from pyogi import BaseText


class Multiline(Widget, geometry.Rect):

    def __init__(self, text, font, from_side="topleft", too_side="bottomleft",
                 offset=(0, 0), **config):
        """Mehrzeiliger Text Widget"""

        Widget.__init__(self, **config)

        self.font = font
        self._text = text
        self._from_side = from_side
        self._too_side = too_side
        self._offset = offset
        self.rect = pygame.Rect((0, 0), (0, 0))
        self.text_list = self.create_text_list(self._text.splitlines())

    def create(self):

        Widget.create(self)
        for widget in self.text_list:
            widget.change(-1)

    def update(self):

        if self.text_list:
            sort.row(self.text_list, pos=self.get_side(self._from_side),
                           offset=self._offset, from_side=self._from_side,
                           too_side=self._too_side)
            self.rect = sizes.bound_rect(self.text_list)

    def draw(self, buffer):

        Widget.draw(self)
        if self.visibility:
            for text in self.text_list:
                text.draw(buffer)

    def create_text_list(self, liste):
        """Macht aus einer liste Strings eine liste mit Base_text Widget"""

        text_list = []
        for text in liste:
            widget = BaseText(text, self.font, **self._config)
            text_list.append(widget)
            self.add(widget)
        return text_list

    @property
    def text(self):

        return self._text

    @text.setter
    def text(self, value):

        self._text = value
        self.text_list = self.create_text_list(self._text.splitlines())
        self.change(2)

    def add_row(self, text):
        """Fuegt ans ende eine neue Zeile ein"""

        new_text = BaseText(text, self.font, **self._config)
        self._text = "%s\n%s" % (self._text, text)
        self.text_list.append(new_text)
        self.add(new_text)
        if self._theme:
            new_text.create()
        self.change(2)
        return new_text

    def if_in(self, pos):
        """testet ob pos in Widget liegt"""

        try:
            geometry.Rect.if_in(self, pos)
        except MouseIn:
            for text in self.text_list:
                text.if_in(pos)
