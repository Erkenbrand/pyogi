import pygame

from pyogi.basics.widget import Widget
from pyogi.geometry import geometry
from pyogi.image import Imagestore
from pyogi.locals import *


class BaseImage(Widget, geometry.Rect):

    def __init__(self, image):
        """Ein Widget das ein uebergebenes Surface in der Gui representiert"""

        Widget.__init__(self)

        self._image = image
        self.rect = image.get_rect()

    @property
    def image(self):

        return self._image

    @image.setter
    def image(self, value):

        self._image = value
        self.size = value.get_size()

    @geometry.Rect.size.setter
    def size(self, value):

        self.rect.size = value
        self._image = pygame.transform.scale(self._image, value)

    def draw(self, buffer):
        """zeichnet auf das uebergebene Surface"""

        if self.visibility:
            buffer.blit(self._image, self.rect)


class Image(BaseImage):

    def __init__(self, path):
        """
        Das Image wird im Imagestore mit dem nahmen path gespeichert
        """
        BaseImage.__init__(self, Imagestore.get_image(path))

        self._path = path

    def update(self):

        self.image = Imagestore.get_image(self._path)

    @property
    def path(self, value):

        return self._path

    @path.setter
    def path(self, value):

        if value != self._path:
            self._path = value
            self.change(2)

    def draw(self, buffer):
        """
        fuehrt wen benoetigt ein update des Widgets aus
        und zeichnet auf das uebergebene Surface
        """

        Widget.draw(self)
        if self.visibility:
            BaseImage.draw(self, buffer)
