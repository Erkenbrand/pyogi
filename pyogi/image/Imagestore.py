from weakref import WeakValueDictionary

from pygame.image import load


store = WeakValueDictionary()

def get_image(path):
    """Gibt einen Verweis auf das Image zurueck
    und erstellt falls nicht vorhanden einen neuen Eintrag.
    """

    try:
        return store[path]
    except KeyError:
        image = load(path)
        if image.get_alpha() is None:
            image = image.convert()
        else:
            image = image.convert_alpha()
        store[path] = image
        return image