from .event import Event
from .focus import Focus


class Widget(Event, Focus):

    def __init__(self, visibility=True, **config):
        """Basis aller Widget"""

        Event.__init__(self)

        self.visibility = visibility#bestimmt ob Sichtbar oder nicht
        self.changed = 0 #bestimmt ob Upgedatet werden muss
        self._master = None
        self._theme = None
        self._config = config

    def config(self, **vaules):

        self._config.update(vaules)
        self.change(2)

    def share_cofig(self, config):
        self._config = config
        self.change(2)

    def remove_config(self, *vaules):

        hit = False
        for key in vaules:
            if key in self._config:
                hit = True
                del self._config[key]
        if hit:
            self.change(2)

    def create(self):

        self._theme = self._config.get("theme", self._master._theme)

    def update(self):
        pass

    def draw(self):
        """Fuehrt wen benoetigt ein update des Widgets aus."""

        if self.changed:
            if self.changed == 1:
                self.update()
                self.changed = 0
            elif self.changed == 2:
                self.create()
                self.update()
                self.changed = 0

    def togle_visibility(self):
        """Wechselt zwischen sichtbar und unsichtbar."""

        self.visibility = False if self.visibility else True

    def change(self, level):
        """Gibt das aktuelle Widget zum update frei."""

        if level > self.changed:
            try:
                self._master.change(1)
            except AttributeError:
                print("Fuer %s ist noch kein Master gesetzt" % (self))
            self.changed = level
        elif level < 0:
            self.changed = 2

    def add(self, widget):
        """Setzt das widget als Master des uebergebenen."""

        widget._master = self
        widget.change(2)

    def kill(self):
        """Loescht das Widget vom Master."""

        try:
            self._master.remove(self)
            self._master = None
            if self == Focus.active:
                Focus.active = None
        except AttributeError:
            print(str(self) + "ist nicht im Master enthalten")
