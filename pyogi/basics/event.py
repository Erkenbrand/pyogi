from pyogi.locals import *


class Event(object):
    """Basis event object von dem alle widget erben"""

    def __init__(self):
        self.events = {}
        self._active = True

    def bind_event_mode(self, etype, mode, event):
        """
        bindet ein Event an das widget
        etype : ein pygame Event type (MOUSEMOTION, MOUSEBUTTONUP,
        MOUSEBUTTONDOWN, KEYUP, KEYDOWN)
        mode : die art der eingabe
        bei MOUSEBUTTONUP/DOWN button
        bei MOUSEMOTION buttons
        bei KEYUP/DOWN key
        event : das bei Aktivierung aufzurufende Event
        """

        self.events[EVENTSTRING % (etype, mode)] = event

    def bind_event(self, name, event):
        """bindet ein event an das Ereignisname"""

        self.events[name] = event

    def clean_events(self):
        """loescht alle gebundenen events"""

        self.events = {}

    def play_event(self, name):
        """spielt das event mit den event name"""

        if self.visibility and self._active:
            funktion = self.events.get(name, None)
            if funktion:
                funktion()
                return True
        return False

    def key_input(self, event, mod, key, unicode):
        """hier kann eine gesonderte Keyboard Eingabe stattfinden"""

        self.play_event(event)
