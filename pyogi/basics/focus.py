from pyogi.locals import LOST_FOCUS, SET_FOCUS


class Focus:
    """
    Verwaltet die Fokussierung der Widget. Das aktuel fokussierte Widget
    ist in Focus.active gespeichert.
    """

    active = None

    def focussing(self):
        """Fokussiert das Widget,
        defokussiert das aktuelle fokussierte Widget.
        """

        if Focus.active != self:
            try:
                Focus.active.play_event(LOST_FOCUS)
            except AttributeError:
                print("Es wurde kein Widget zum defokussieren gefunden")
            Focus.active = self
            self.play_event(SET_FOCUS)
