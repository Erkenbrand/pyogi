import pygame

from pyogi.basics.widget import Widget
from pyogi.geometry import geometry


class BaseColor(Widget, geometry.Rect):

    def __init__(self, size, color):
        """einfaches einfarben Widget"""

        Widget.__init__(self)
        self._color = color
        self.rect = pygame.Rect((0, 0), size)

    @property
    def color(self):

        return self._color

    @color.setter
    def color(self, vaule):

        self._color = vaule
        self.change(2)

    def draw(self, buffer):
        if self.visibility:
            pygame.draw.rect(buffer, self._color, self.rect)
