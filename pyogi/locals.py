﻿from pygame.locals import *

from pyogi.theme import base

EVENTSTRING = "%s-%s"
MOUSE_GO_OUT = "mouse_go_aut"
MOUSE_GO_IN = "mouse_go_in"
SET_FOCUS = "set_focus"
LOST_FOCUS = "lost_focus"
FOCUS = "focus"
REFLECT_SIDE = {"topleft":"bottomright", "midtop":"midbottom",
             "topright":"bottomleft", "midleft":"midright", "center":"center",
             "midright":"midleft", "bottomleft":"topright",
             "midbottom":"midtop", "bottomright":"topleft"
             }
DEFAULT_THEME = base

class MouseIn(Exception):
    """Gibt das angeklickte Widget durch zur Bearbeitung"""

    def __init__(self, widget, *vaule):
        self.widget = widget

    def __str__(self):
        return "Event fuer " + str(self.widget) + " active"
