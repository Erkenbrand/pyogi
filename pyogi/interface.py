import pygame

from pyogi.locals import *
from pyogi.basics import Focus


class Interface():

    def __init__(self, size, theme=DEFAULT_THEME):
        """Beinhaltet alle anderen Widget"""

        self._theme = theme
        self._slaves = []
        self.rect = pygame.Rect((0, 0), size)
        self.changed = 0
        self.mouse_in = self
        self.mouse_pos = pygame.mouse.get_pos()

    def config(self, **vaules):

        self._theme = vaules.get("theme", DEFAULT_THEME)
        for widget in self._slaves:
            widget.change(-1)

    @property
    def x(self):
        return self.rect.x

    @property
    def y(self):
        return self.rect.y

    def update(self):
        """Fuert das FOCUS event aus"""

        if Focus.active:
            Focus.active.play_event(FOCUS)

    def play_event(self, name):
        """None Methode"""
        pass

    def mouse_motion(self, buttons, pos, rel):
        """
        Speichert die aktuelle Mausposition und ruft dann einen mouse_test auf,
        in dem das Event (MOUSEMOTION - buttons) uebergeben wird
        """

        self.mouse_pos = pos
        self.mouse_test(buttons).play_event(EVENTSTRING % (MOUSEMOTION,
                                                           buttons))

    def mouse_test(self, buttons):
        """
        Testet in welchen Widget die Maus ist,
        ist die Maus zum ersten mal im Widget dann wird das Event MOUSE_GO_IN
        und (MOUSE_GO_IN - buttons) aufgerufen. verlaest die Maus ein Widget,
        dann wird das Event MOUSE_GO_OUT aufgerufen.
        In dem Widget wo die Maus ist wird ein Event buttons aufgerufen z.b. (1,0,0)
        """
        try:
            for widget in reversed(self._slaves):
                widget.if_in(self.mouse_pos)
            raise MouseIn(self)
        except MouseIn as container:
            go_in = container.widget
            if_in = self.mouse_in
            if if_in != go_in:
                if_in.play_event(MOUSE_GO_OUT)
                go_in.play_event(MOUSE_GO_IN)
                go_in.play_event(EVENTSTRING % (MOUSE_GO_IN, buttons))
                self.mouse_in = go_in
            if_in.play_event(buttons)
            return go_in

    def mouse_imput(self, name, button):
        """
        Vuert ein Maus Event aus.
        name : MOUSEBUTTONUP, MOUSEBUTTONDOWN
        button : 1, 2, 3, 4, 5
        ruft mouse_test auf
        """

        event = EVENTSTRING % (name, button)
        mouse_pres = pygame.mouse.get_pressed()
        self.mouse_test(mouse_pres).play_event(event)

    def change(self, level):
        """None Methode"""

        self.changed = level

    def relativ_pos(self, pos):
        """gibt die Position von Pos im Widget zurueck"""

        return pos

    def key_input(self, mod, key, unicode):
        """
        Vuert eine tastertureingabe aus.
        mod : pygame events KEYDOWN, KEYUP
        key : die nummer der Taste
        unicode : Taste als Schriftzeichen
        """

        if Focus.active:
            Focus.active.key_input(EVENTSTRING % (mod, key), mod, key, unicode)

    def add(self, widget):
        """Fuegt das widget dem Interface hinzu"""

        self._slaves.append(widget)
        widget._master = self
        widget.change(2)

    def relativ_master_pos(self, pos):
        """gibt die Position von Pos im Widget zurueck"""

        return (pos[0] - self.rect.x, pos[1] - self.rect.y)

    def relativ_side(self, side="center"):
        """gibt die Position von einner Seite im Widget zurueck"""

        return self.get_side(side)

    def place(self, widget, pos=(0, 0), side="topleft", anchor=None):
        """
        Platziert das Widget an der uebergebenen Position
        side : gibt die Seite an die platziert wird
        ankre : gibt die Position des Containers an
                von der aus das Widget platziert wird
        """

        self.add(widget)
        x, y = self.get_side(anchor) if anchor else self.get_side(side)
        widget.set_side((pos[0] + x, pos[1] + y), side)

    def resize(self, size):
        """Setzt die Groesse des Interface neu"""

        self.rect.size = size

    def get_side(self, side):
        """Gibt die Position des Interface ueber eine Seite aus"""

        return getattr(self.rect, side)

    def draw(self, buffer):
        """Zeichnet alle gui Elemente auf das uebergebene Surface"""

        self.changed = 0
        for widget in self._slaves:
            widget.draw(buffer)

    def clean(self):
        """Loescht alle Widget"""

        for widget in self._slaves[:]:
            widget.kill()
            