import pygame

from pyogi.basics import Widget
from pyogi.geometry import geometry
from pyogi import BaseText, Multiline
from pyogi.locals import DEFAULT_THEME


class Label(Widget, geometry.Rect):

    def __init__(self, text, font, size=None, orientation="center", **config):
        """Einzeiliges Text Widget auf einer Farbe abgebildet"""

        Widget.__init__(self, **config)

        self._text = BaseText(text, font, **config)
        if size:
            self.rect = pygame.Rect((0, 0), size)
        else:
            self.rect = pygame.Rect((0, 0), self._text.size)
        self._text.set_side(self.get_side(orientation), orientation)
        self.add(self._text)
        self._orientation = orientation

    def create(self):
        Widget.create(self)

        self._color = self._config.get("widget_bg",
                                       self._theme.colors.widget_bg)
        if self._color:
            self._image = pygame.Surface(self.size)
        else:
            self._image = pygame.Surface(self.size).convert_alpha()
        self._text.change(-1)

    def update(self):

        self._image.fill(self._color if self._color else (0, 0, 0, 0))
        self._text.draw(self._image)

    def draw(self, buffer):

        Widget.draw(self)
        if self.visibility:
            buffer.blit(self._image, self.rect)

    @property
    def font(self):
        return self._text.font

    @font.setter
    def font(self, font):
        self._text.font = font
        self.change(2)

    @property
    def text(self):
        return self._text.text

    @text.setter
    def text(self, text):
        self._text.text = text
        self.change(2)

    @property
    def orientation(self):
        return self._orientation

    @orientation.setter
    def orientation(self, orientation):
        self._orientation = orientation
        self._text.set_side(self.get_side(orientation), orientation)
        self.change(2)

    @geometry.Rect.size.setter
    def size(self, vaule):
        self.rect.size = vaule if vaule else self._text.size
        self.change(2)
