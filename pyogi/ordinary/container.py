import pygame

from pyogi.locals import *
from pyogi.basics import Widget
from pyogi.geometry import geometry


class BaseContainer(Widget):

    def __init__(self, **config):
        """
        Ein Container fuer Widget, er zeichnet die Widget an dem Ort,
        an dem sie mit der Methode place platziert werden
        und in der reihen folge in der sie hinzugefuegt wurden
        """

        Widget.__init__(self, **config)

        self._slaves = []

    def update(self):
        pass

    def draw(self, buffer):
        """
        Fuehrt wen benoetigt ein update des Widgets aus
        und zeichnet auf das uebergebene Surface.
        """

        for slave in self._slaves:
            slave.draw(buffer)

    def key_input(self, event, mod, key, unicode):
        """
        Fuehrt eine tastertureingabe aus.
        name : pygame events KEYDOWN, KEYUP
        key : die nummer der Taste
        """

        Widget.key_input(self, event, mod, key, unicode)
        if self._focus:
            self._focus.key_input(event, mod, key, unicode)

    def add(self, widget):
        """Fuegt das uebergebene widget dem Container hinzu."""

        self._slaves.append(widget)
        Widget.add(self, widget)

    def remove(self, widget):
        """Loescht das uebergebenen Widget aus dem Container."""

        self._slaves.remove(widget)
        self.change(2)
        if self._focus == widget:
            if self._slaves:
                self.set_focus(self._slaves[0])

    def test_slave(self, pos):
        """Prueft ob die Position in den slaves ist."""

        for slave in reversed(self._slaves):
            slave.if_in(pos)

    def if_in(self, pos):
        """Prueft ob die Position im Container ist und in den slaves."""

        try:
            geometry.Rect.if_in(self, pos)
        except MouseIn:
            self.test_slave(self.mouse_pos)
            raise

    def kill(self):
        """Loescht alle Widget und den Container."""

        Widget.kill(self)
        for widget in self._slaves[:]:
            widget.kill()

    def clean(self):
        """Loescht alle Widget."""

        self.change(2)
        for widget in self._slaves[:]:
            widget.kill()

class Container(BaseContainer, geometry.Rect):

    def __init__(self, size=(128, 128), **config):
        """
        Ein Container fuer Widget, er zeichnet die Widget an dem Ort,
        an dem sie mit der Methode place platziert werden
        und in der reihen folge in der sie hinzugefuegt wurden
        """

        BaseContainer.__init__(self, **config)

        self._color = None
        self._image = None
        self.rect = pygame.Rect((0, 0), size)

    def create(self):

        BaseContainer.create(self)
        self._color = self._config.get("widget_bg",
                                       self._theme.colors.widget_bg)
        self._image = pygame.Surface(self.size)
        for widget in self._slaves:
            widget.change(-1)

    def update(self):

        self._image.fill(self._color)
        BaseContainer.draw(self, self._image)

    def draw(self, buffer):
        """
        fuehrt wen benoetigt ein update des Widgets aus
        und zeichnet auf das uebergebene Surface
        """

        Widget.draw(self)
        if self.visibility:
            buffer.blit(self._image, self.rect)

    def place(self, widget, pos=(0, 0), side="topleft", anchor=None):
        """
        platziert das Widget an der uebergebenen Position
        side : gibt die Seite an die platziert wird
        anchor : gibt die Seite des Containers an,
        von der aus das Widget platziert wird
        """

        self.add(widget)
        x, y = self.get_side(anchor) if anchor else self.get_side(side)
        widget.set_side((pos[0] + x - self.x, pos[1] + y - self.y), side)
