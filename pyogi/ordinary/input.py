from pyogi import Label
from pyogi.locals import *


class Entry(Label):

    def __init__(self, font, size=(64, 16), **config):
        """
        ein einfaches Eingabefeld das ueber den property text
        ausgelesen werden kann
        """

        Label.__init__(self, "", font, size, "midleft", **config)

        self._verzoegerung = 20 #verzoegert die Eingabeschleife
        self.time = 0
        self._last_input = None
        self._cursorset = False
        self._cursorpos = 0
        self.cursor_time = 20
        self._cursor_timer = self.cursor_time
        self.bind_event_mode(MOUSEBUTTONUP, 1,
                             lambda: self.set_cursor(self.relativ_master_pos(
                                self._master.mouse_pos)))
        self.bind_event_mode(KEYDOWN, K_BACKSPACE, self.del_cursor)
        self.bind_event_mode(KEYDOWN, K_RIGHT, lambda: self.move_cursor(1))
        self.bind_event_mode(KEYDOWN, K_LEFT, lambda: self.move_cursor(-1))
        self.bind_event(SET_FOCUS, self.activate_cursor)
        self.bind_event(FOCUS, self.blink_cursor)
        self.bind_event(LOST_FOCUS, self.disable_cursor)
        self.disabled_buttons = {K_RETURN, K_TAB}

    def update(self):

        if self._cursorset:
            old_text = self.text
            self._text.text = "%s|%s" % (old_text[:self._cursorpos],
                                         old_text[self._cursorpos:])
            Label.update(self)
            self._text.text = old_text
        else:
            Label.update(self)

    @property
    def text(self):
        return self._text.text

    @text.setter
    def text(self, st):
        if self._text.font.get_rect(st).w < self.rect.w:
            self._text.text = st
            self.change(1)
        else:
            print("text zu lang")

    @property
    def cursorset(self):
        return self._cursorset

    @cursorset.setter
    def cursorset(self, value):
        self._cursorset = value
        self.change(1)

    def move_cursor(self, way):
        """Bewegt den eingabezeiger nach rechts (+) oder nach links (-)"""

        new_pos = self._cursorpos + way
        self._cursorpos = (new_pos if 0 <= new_pos <= len(self.text)
                           else self._cursorpos)
        self._cursor_timer = self.cursor_time
        self.cursorset = True

    def set_cursor(self, pos):
        """Setzt den Cursor an die angegebenen Position"""

        self._cursor_timer = self.cursor_time
        self.cursorset = True
        self.focussing()
        if self.text:
            pos_x = pos[0]
            cursorpos = 0
            width = 0
            for letter_size in self._text.font.get_metrics(self.text):
                half_width = letter_size[4] / 2
                width += half_width
                if pos_x < width:
                    self._cursorpos = cursorpos
                    return
                width += half_width
                cursorpos += 1
                if pos_x < width:
                    self._cursorpos = cursorpos
                    return
            self._cursorpos = len(self.text)
        else:
            self._cursorpos = 0

    def activate_cursor(self):
        """Aktiviert den Cursor"""

        self.cursorset = True

    def disable_cursor(self):
        """Deaktiviert den Cursor"""

        self._cursor_timer = self.cursor_time
        self.cursorset = False

    def blink_cursor(self):
        """Wird an ein FOCUS event gebunden um den Cursor blinken zu lassen"""

        if self._cursor_timer == 0:
            self.cursorset = not self._cursorset
            self._cursor_timer = self.cursor_time
        else:
            self._cursor_timer -=1

    def write_cursor(self, st):
        """
        Fuegt einen string an der stelle des Cursors ein
        und verschiebt in um die Laenge des strings
        """

        text = self.text
        text = "%s%s%s" % (text[:self._cursorpos], st, text[self._cursorpos:])
        if self._text.font.get_rect(text).w < self.rect.w:
            self._cursorpos += len(st)
            self.text = text

    def del_cursor(self):
        """Loescht die Eingabe hinter dem Cursor"""

        if self._cursorpos > 0:
            text = self.text
            end = text[self._cursorpos:]
            self._cursorpos -= 1
            self.text = text[:self._cursorpos] + end

    def key_input(self, event, mod, key, unicode):
        """Behandelt die Tastatur Eingabe bei Fokus"""

        self.time -= 1
        if key in self.disabled_buttons:
            return
        if mod == KEYDOWN:
            if self._last_input == key:
                if self.time < 0:
                    if not self.play_event(event):
                        self.write_cursor(unicode)
            else:
                if unicode:
                    if not self.play_event(event): 
                        self.write_cursor(unicode)
                    self.time = self._verzoegerung
                    self._last_input = key
                else:
                    self.play_event(event)
                    self.time = self._verzoegerung
                    self._last_input = key
        else:
            if self._last_input == key:
                self._last_input = None
                self.play_event(event)
