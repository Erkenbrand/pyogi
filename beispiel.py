#!/usr/bin/env python3

"""pyogi Test und Beispiel"""

import sys

import pygame
from pygame.freetype import Font, SysFont

import pyogi
from pyogi.locals import * # pygame.locals werden mit importiert


#init der pygame Umgebung
pygame.init()
pygame.key.set_repeat(1, 1) #sollte immer gesetzt werden

buffer = pygame.display.set_mode((600, 600))
CLOCK = pygame.time.Clock()
FPS = 30

#Erstelle ein Standard Font Objekt fuer die Gui
#Es muss ein freetype Font sein
DEFAULTFONT = SysFont(None, 12)
DEFAULTFONT.pad = True

DEFAULTFONTVERTIVAL = SysFont(None, 12)
DEFAULTFONTVERTIVAL.vertical = True
DEFAULTFONTVERTIVAL.pad = True

#Das Haupt Fenster der Gui mit der Groeße des ganzen Fensters
interface = pyogi.Interface(buffer.get_size())

#Erstellung aller anderen Gui Elemente
container = pyogi.Container(size=(160, 160))
color_test = pyogi.BaseColor((40, 40), (255, 0, 0))
color_test2 = pyogi.BaseColor((40, 40), (0, 255, 0))
text_test = pyogi.BaseText("EXIT", font=DEFAULTFONT, text_fg=(255, 0, 0),
                 text_bg=(255, 255, 255))
text_test2 = pyogi.BaseText("Test", font=DEFAULTFONT, text_bg=(255, 255, 255))
mehrzeiliger_text = pyogi.Multiline("das ist zeile 1\nund das ist zeile 2\n______",
                             DEFAULTFONT, text_fg=(255, 255, 255),
                             from_side="midtop", too_side="midbottom")
entry_test = pyogi.Entry(font=DEFAULTFONT)
entry_text = pyogi.BaseText("Entry", font=DEFAULTFONT,
                              text_fg=(255, 255, 255), text_bg=(100, 100, 155))
entry_test2 = pyogi.Entry(font=DEFAULTFONT, widget_bg=(255, 255, 255))
entry_text2 = pyogi.BaseText("Entry", font=DEFAULTFONT,
                               text_fg=(255, 255, 255), text_bg=(100, 100, 155))
liste = [pyogi.BaseText("das ist text " + str(num), DEFAULTFONT,
                  text_fg=(255, 255, 0)) for num in range(0, 6)]

#einstellen der event eingaben
text_test.bind_event_mode(MOUSEBUTTONUP, 1, sys.exit)
text_test.bind_event(MOUSE_GO_IN,
                     lambda: (setattr(text_test, "bg", (100, 100, 155))))
text_test.bind_event(MOUSE_GO_OUT,
                     lambda: setattr(text_test, "bg", (255, 255, 255)))

#setzt den Fokus 4der Gui auf das Widget wen es angeklickt wird
container.bind_event_mode(MOUSEBUTTONUP, 1, container.focussing)

#Ruft eine Funktion auf bei Fokussierung
container.bind_event(SET_FOCUS, lambda: print("focus set at container"))
container.bind_event(LOST_FOCUS, lambda: print("container lost focus"))

#platziert alle Widget die ins Interface sollen
interface.place(container, side="center")
interface.place(text_test2, side="bottomright")
interface.place(entry_test, side="topright")

#platziert alle Widget die in den Container sollen
container.place(color_test)
container.place(color_test2, side="bottomright")
container.place(text_test, side="center")
container.place(entry_test2, side="midbottom")

#platziert anliegend an ein anderes Widget
pyogi.place.too(container, mehrzeiliger_text, offset=(0, 0))
pyogi.place.too(entry_test, entry_text, side="midright", offset=(-2, 0))
pyogi.place.too(entry_test2, entry_text2, side="midright", offset=(-2, 0))

#platziert die Widget in der liste in einer reihe
pyogi.place.in_row(interface, liste, (0, 0), offset=(0, 10))

if __name__ == '__main__':

    while True:
        for event in pygame.event.get():
            etype = event.type
            if etype == QUIT:
                sys.exit()
            elif etype == MOUSEMOTION:
                #muss immer aufgerufen werden
                interface.mouse_motion(event.buttons, event.pos, event.rel)
            elif etype == MOUSEBUTTONUP:
                #muss immer aufgerufen werden
                interface.mouse_imput(etype, event.button)
            elif etype == MOUSEBUTTONDOWN:
                #muss immer aufgerufen werden
                interface.mouse_imput(etype, event.button)
            elif etype == KEYDOWN:
                #muss immer aufgerufen werden
                interface.key_input(etype, event.key, event.unicode)
            elif etype == KEYUP:
                #muss immer aufgerufen werden
                interface.key_input(etype, event.key, None)
            if etype == VIDEORESIZE:
                pass

        buffer.fill((0, 0, 0))

        #muss immer aufgerufen werden
        interface.update()
        interface.draw(buffer)#buffer ist das Surface auf das die Gui zeichnet

        past_time = CLOCK.tick(FPS)
        pygame.display.flip()